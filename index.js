import { NativeModules, Platform } from 'react-native';

const { ReactNativeAnalyticCampaign } = NativeModules;

export const Campaign = Platform.select({
  android: {
    campaign: NativeModules,
  }
})

export default ReactNativeAnalyticCampaign;
