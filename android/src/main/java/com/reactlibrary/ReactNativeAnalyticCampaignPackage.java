package com.reactlibrary;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.bridge.JavaScriptModule;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import android.app.Application;
import android.content.Context;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import java.util.HashMap;
import com.google.android.gms.analytics.HitBuilders;

public class ReactNativeAnalyticCampaignPackage implements ReactPackage {
    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        return Arrays.<NativeModule>asList(new ReactNativeAnalyticCampaignModule(reactContext));
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }
    private final String PROPERTY_ID;

    public ReactNativeAnalyticCampaignPackage(ReactApplicationContext reactContext, String propertyId) {
        super(reactContext);
        PROPERTY_ID = propertyId;
    }

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
            : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                : analytics.newTracker(R.xml.ecommerce_tracker);
        mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }

    @ReactMethod
    public void campaign(String campaignData) {
        Tracker t = getTracker(TrackerName.APP_TRACKER);

        // Campaign data sent with this hit.
        t.send(new HitBuilders.ScreenViewBuilder()
        .setCampaignParamsFromUrl(campaignData)
        .build()
        );
    }
}
